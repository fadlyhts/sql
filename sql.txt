NOMOR 1 :
- CREATE DATABASE fadlishop;


NOMOR 2 : 
- CREATE TABLE users (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
    email varchar(255),
    password varchar(255),
    PRIMARY KEY (id)
); 

- CREATE TABLE items(
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
    description varchar(255),
    price int,
    stock int, 
    category_id int,
    FOREIGN KEY (category_id) REFERENCES categories(id),
    PRIMARY KEY (id)
); 

- CREATE TABLE categories(
    id int NOT NULL AUTO_INCREMENT,
    name varchar(255),
    PRIMARY KEY (id)

); 



NOMOR 3 :
- INSERT INTO users (name, email, password)
VALUES ('John Doe', 'john@doe.com', 'john123'),
('Jane Doe', 'jane@doe.com', 'jenita123');


- INSERT INTO categories (name)
VALUES ('gadget'),
('cloth'),
('men'),
('women'),
('branded');


- INSERT INTO items (name, description, price, stock, category_id)
VALUES ('Sumsang b50', 'hape keren dari merek sumsang', '4000000', '100', '1'),
('Uniklooh', 'baju keren dari brand ternama', '500000', '50', '2'),
('IMHO Watch', 'jam tangan anak yang jujur banget', '2000000', '10', '1');



NOMOR 4:
a. SELECT id, name, email FROM users; 

b. - SELECT*FROM items WHERE price>1000000,
   - SELECT*FROM items WHERE name LIKE 'Uniklooh';

c. SELECT categories.name, items.name, items.description, items.price, items.stock, items.category_id
FROM categories
INNER JOIN items ON categories.name=items.kategori;



NOMOR 5:
- UPDATE items
SET price = 2500000
WHERE id=2;
